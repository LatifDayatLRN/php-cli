<?php

namespace App;

class File
{
    public  $file,
            $handle,
            $dataFromFile,
            $dataToSave;

    public function __construct ($file)
    {
        $this->file = $file;
    }

    protected function setFile ($file)
    {
        $this->file = $file;
        return $this;
    }

    protected function setDataFromFile ()
    {
        $this->dataFromFile = file($this->file);
        return $this;
    }

    protected function getDataFromFile ()
    {
        return $this->setDataFromFile()->dataFromFile;
    }

    protected function setDataToSave ($input)
    {
        $this->dataToSave = $input;
        return $this;
    }

    protected function getDataToSave ()
    {
        return $this->dataToSave;
    }

    protected function setHandle ()
    {
        fopen($this->getFile(), 'c+');
        return $this;
    }

    protected function getHandle ()
    {
        return $this->setHandle()->handle;
    }

    public function open ()
    {
        $this->setHandle();
        return $this;
    }

    public function read()
    {
        return $this->getDataFromFile();
    }

    public function write ($text)
    {

        fwrite(fopen('./data','a'), $text);
        return $this;
    }
}

?>
