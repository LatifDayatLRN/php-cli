<?php

namespace App;

class Statistic
{
    protected   $last,
                $name,
                $mark,
                $highest,
                $lowest,
                $passed,
                $notPassed;
    protected   $table = array(),
                $newTable = array(),
                $markTable = array(),
                $nameTable = array();
    
//    public function __construct($data)
//    {
//        $this->setTable($data);
//    }
    
    public function setTable($data)
    {
        $table = array_merge($this->table ,$data);
        $this->table = $table;
//        var_dump($this->table);
        return $this;
    }
    
    public function getTable()
    {
        ksort($this->table);
        return $this->table;
    }
    
    protected function setMarkTable ($data)
    {
        $this->markTable[] = $data;
        return $this;
    }
    
    public function getMarkTable ()
    {
        return $this->markTable;
    }
    
    protected function setNameTable ($data)
    {
        $this->nameTable[] = $data;
        return $this;
    }
    
    public function getNameTable ()
    {
        return $this->nameTable;
    }
    
    protected function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    protected function setMark($mark)
    {
        $this->mark = $mark;
        return $this;
    }
    
    public function getMark()
    {
        return $this->mark;
    }
    
    public function setHighest ()
    {
        $table = $this->getMarkTable();
//        die(var_dump($table));
        $mark = max($table);
//        $markRow = array_values($markRow);
//        var_dump($markRow);
        $this->highest = array_search($mark, $this->getTable());
        return $this;
    }
    
    public function getHighest ()
    {
        return $this->setHighest()->highest;
    }
    
    public function setLowest ()
    {
        $table = $this->getMarkTable();
        $mark = min($table);
        
        $this->lowest = array_search($mark, $this->getTable());
        return $this;
    }
    
    public function getLowest ()
    {
        return $this->setLowest()->lowest;
    }
    
    protected function setPassed ($data)
    {
        $this->passed = $data;
        return $this;
    }
    
    public function getPassed ()
    {
        return $this->passed;
    }
    
    protected function setNotPassed ($data)
    {
        $this->notPassed = $data;
        return $this;
    }
    
    public function getNotPassed ()
    {
        return $this->notPassed;
    }
    
    public function append () 
    {
        $mark = $this->getMark();
        $name = $this->getName();
//        var_dump($name);
        $this->setNameTable($name);
        $this->setMarkTable($mark);
        
        $row = array($name => $mark);    
        $this->setTable($row);
        
        return $this;
    }
    
    public function newData ($name, $mark)
    {
        $this->setName($name)->setMark($mark)->append();
    }
    
    public function correction ()
    {
        $table = $this->getTable();
//        var_dump($table);
        arsort($table);
//        var_dump($table);
        $passed = array();
        $notPassed = array();
        foreach ($table as $name => $mark) :
//                die(var_dump($name));
                if ($mark > 5) :
                    $mark = ($mark * 100) / 10;
                    $text1 = 'Nilai Ujian '.$name.' telah mencukupi.';
                    $text2 = ' Capaian '.$mark.'%';
                    $passed[] = $text1.$text2;
                else :
                    $mark = ($mark * 100) / 10;
                    $text1 = 'Nilai Ujian '.$name.' tidak mencukupi.';
                    $text2 = ' Capaian '.$mark.'%';
                    $notPassed[] = $text1.$text2;
                endif;           
        endforeach;
        
        $this->setPassed($passed);
        $this->setNotPassed($notPassed);
        return $this;
    }
    
}

?>