<?php
require_once 'app.php';
require_once 'statistic.php';

use App\App;
use App\Statistic; 

$app = new App;
$statistic = new Statistic;

echo '==>> Selamat Datang di Program Input Nilai Ujian <<==';
echo "\n";

do {
    echo 'Masukkan Nama Peserta : ';

    $name = $app->askName()->getName() ;

    if ($name != 'n' && $name != null ) {

        echo 'Masukkan Nilai '.$app->getName().' : ';

        $mark = $app->askMark()->getMark();
        if ($mark == 'n' || $mark == null ) :
            continue;
        endif;

        $statistic->newData($name, $mark);
        echo 'Nilai '.$name.' adalah '.$mark;
        echo "\n";

    } else {
        continue;
    }


    echo 'Lanjutkan ? [y/n] : ';
    $app->askme();

    if ($app->answer() == 'y' or $app->answer() == null) :
        continue;
    else :
        break;
    endif;
}
while (true);
echo "\n";

for ($x=1;$x<=100;$x++) :
    if ($x==49) :
        echo '*';
    else :
        echo '-';
    endif;
endfor;
echo "\n";

$table = $statistic->getTable();
//var_dump($table);
foreach ($table as $key => $value) :
    echo $key.' => '.$value;
    echo "\n";
endforeach;



$statistic->correction();

echo '==>Nilai Lulus<==';
echo "\n";
foreach ($statistic->getPassed() as $value) :
    echo $value;
    echo "\n";
endforeach;

echo '==>Nilai Tidak Lulus<==';
echo "\n";
foreach ($statistic->getNotPassed() as $value) :
    echo $value;
    echo "\n";
endforeach;


echo "\n".'Nilai TERTINGGI adalah '.$statistic->getHighest();
echo ' dengan nilai '.$table[$statistic->getHighest()]."\n";
echo 'Nilai TERENDAH adalah '.$statistic->getLowest();
echo ' dengan nilai '.$table[$statistic->getLowest()]."\n";

echo "\n".'Program Dibuat dengan bahasa PHP oleh Latif Hidayatullah'."\n";
?>